const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/IndexPage.vue") },
      { path: "dashboard", component: () => import("pages/DashboardPage.vue") },
      {
        path: "credential",
        component: () => import("pages/CredentialPage.vue"),
      },
      { path: "profile", component: () => import("pages/ProfilePage.vue") },
      { path: "documents", component: () => import("pages/DocumentsPage.vue") },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
